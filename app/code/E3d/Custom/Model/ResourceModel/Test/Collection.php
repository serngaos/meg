<?php
namespace E3d\Custom\Model\ResourceModel\Test;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Remittance File Collection Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('E3d\Custom\Model\Test', 'E3d\Custom\Model\ResourceModel\Test');
    }
}