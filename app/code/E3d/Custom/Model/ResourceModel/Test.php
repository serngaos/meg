<?php

namespace E3d\Custom\Model\ResourceModel;



use \Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Test extends AbstractDb
{
    /**
     * Test Abstract Resource Constructor
     * @return void
     */
    protected function _construct()
    {
        $this->_init('testing', 'id');
    }
}