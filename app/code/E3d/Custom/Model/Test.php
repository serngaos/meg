<?php

namespace E3d\Custom\Model;


use E3d\Custom\Api\Data\TestInterface;
use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;


/**
 * Class File
 * @package E3d\Custom\Model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Test extends AbstractModel implements  TestInterface,IdentityInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'testing';

    /**
     * Test Initialization
     * @return void
     */
    protected function _construct()
    {
        $this->_init('E3d\Custom\Model\ResourceModel\Test');
    }


    /**
     * Get Name
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->getData(self::Name);
    }

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return $this->getData(self::Create_at);
    }

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::Id);
    }

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        return $this->setData(self::Name, $name);
    }



    /**
     * Set Created At
     *
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::Create_at, $createdAt);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::Id, $id);
    }


}