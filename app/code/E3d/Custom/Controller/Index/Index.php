<?php
namespace E3d\Custom\Controller\Index;


use Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $_pageFactory;
    protected  $_testFactory;


    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \E3d\Custom\Model\TestFactory $testFactory
    ) {
        $this->_pageFactory = $pageFactory;
        $this->_testFactory = $testFactory;
        return parent::__construct($context);
    }


    public function execute()
    {
        /*$test = $this->_testFactory->create();
        $collection = $test->getCollection();
        foreach($collection as $item){
            echo "<pre>";
            print_r($item->getData());
            echo "</pre>";
        }
        exit();*/
        return $this->_pageFactory->create();


    }

}