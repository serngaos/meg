<?php
namespace E3d\Custom\Block;
use E3d\Custom\Model\TestFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;


class Index extends Template
{
    protected $_testFactory;

    public function __construct(
        Context $context,
        TestFactory $testFactory


    )
    {
        $this->_testFactory = $testFactory;
        parent::__construct($context);

    }

    public function sayHello()
    {
        return __('Hello World');
    }
    public function getTestCollection(){
        $test = $this->_testFactory->create();
        return $test->getCollection();
    }
}