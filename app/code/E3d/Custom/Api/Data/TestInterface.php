<?php
namespace E3d\Custom\Api\Data;

interface TestInterface{

    const Id = 'id';
    const Name = 'name';
    const Create_at= 'create_at';

    public function getName();


    public function getCreatedAt();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Title
     *
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * Set Crated At
     *
     * @param int $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);


}
